(function() {

    // Remove no-js from html tag
    document.documentElement.classList.remove('no-js');

    // Menu toggle
    for (var i = 0; i < document.getElementsByClassName('js-menu-toggle').length; i++) {
        var element = document.getElementsByClassName('js-menu-toggle')[i];

        element.addEventListener('click', function(e) {
            var menu = this.parentNode.parentNode;

            if (this.parentNode.classList.contains('menu') || this.parentNode.classList.contains('language')) {
                e.preventDefault();
                menu.classList.toggle('opened');
            } else {
                menu.classList.remove('opened');
            }
        });

    };

})();